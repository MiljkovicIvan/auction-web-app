// <auto-generated />
namespace WebApplication1.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class all_in_one : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(all_in_one));
        
        string IMigrationMetadata.Id
        {
            get { return "201609162021287_all_in_one"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
