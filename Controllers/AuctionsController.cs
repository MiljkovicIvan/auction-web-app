﻿using Microsoft.AspNet.SignalR;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Hubs;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AuctionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private List<string> parseSearchString(string s)
        {
            List<string> list = new List<string>();

            if (s == null)
                return list;

            const char separator = ' ';
            string [] words = s.Split(separator);

            for(int i = 0; i < words.Length; i++)
            {
                list.Add(words[i]);
            }

            return list;
        }

        // GET: Auctions
        public ActionResult Index(string name, string state, string min, string max, int? page)
        {
            List<string> list = parseSearchString(name);

            if (User.IsInRole("Admin"))
                ViewBag.Role = "Admin";
            else
            {
                if (User.Identity.Name.Equals(""))
                    ViewBag.Role = "NoAuth";
                else
                    ViewBag.Role = "User";
            }

            long minPrice = 0;
            long maxPrice = 0;

            if (!String.IsNullOrEmpty(max))
                maxPrice = long.Parse(max);

            if (!String.IsNullOrEmpty(min))
                minPrice = long.Parse(min);

            var states = new List<String>();
            states.Add("DRAFT");
            states.Add("READY");
            states.Add("OPEN");
            states.Add("SOLD");
            states.Add("EXPIRED");

            ViewBag.State = new SelectList(states);

            var auctions = from m in db.Auctions select m;

            if (!(User.IsInRole("Admin")))
            {
                auctions = auctions.Where(s => !(s.state == AuctionState.READY));
            }

            if (list.Count != 0)
            {
                foreach(string n in list)
                {
                    auctions = auctions.Where(s => s.productName.Contains(n));
                }
            }

            if (!String.IsNullOrEmpty(state))
            {
                if (state == "OPEN")
                    auctions = auctions.Where(s => s.state == AuctionState.OPEN);
                else if (state == "READY")
                    auctions = auctions.Where(s => s.state == AuctionState.READY);
                else if (state == "SOLD")
                    auctions = auctions.Where(s => s.state == AuctionState.SOLD);
                else if (state == "EXPIRED")
                    auctions = auctions.Where(s => s.state == AuctionState.EXPIRED);
                else if (state == "DRAFT")
                    auctions = auctions.Where(s => s.state == AuctionState.DRAFT);
            }

            if (!String.IsNullOrEmpty(min))
                auctions = auctions.Where(s => s.starting_price >= minPrice);
            if (!String.IsNullOrEmpty(max))
                auctions = auctions.Where(s => s.starting_price <= maxPrice);

            int pagination = 10; // when 10 it shows 9 per page
            int pagenumber = (page ?? 1);

            return View(auctions.OrderByDescending(s => s.create_datetime).ToPagedList(pagenumber, pagination));
        }

        // GET: Auctions/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);

            if(auction == null)
            {
                return HttpNotFound();
            }

            var bids = from m in db.Bids select m;
            bids = bids.Where(s => s.idAuction == id).OrderBy(k => k.create_datetime);

            List<string> bidUsersList = new List<string>();

            bidUsersList = bids.Select(u => u.idUser.ToString()).ToList();
            bidUsersList.Reverse();

            AuctionDetailModelView auctionDetailModelView = new AuctionDetailModelView();
            auctionDetailModelView.auction = auction;

            for (int i = 0; i < 10 && bidUsersList.Count > i; i++)
            {
                ApplicationUser user = db.Users.Find(bidUsersList[i]);
                auctionDetailModelView.bids[i] = user.UserName;
            }

            return View(auctionDetailModelView);
        }

        // GET: Auctions/Create
        [System.Web.Mvc.Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Auctions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,productName,duration,starting_price,create_datetime,open_datetime,close_datetime,deleted,state,increment,imageToUpload")] Auction auction)
        {
            if (ModelState.IsValid)
            {
                if (auction.imageToUpload != null)
                {
                    auction.image = new byte[auction.imageToUpload.ContentLength];
                    auction.imageToUpload.InputStream.Read(auction.image, 0, auction.image.Length);
                }
                auction.create_datetime = DateTime.Now;
                auction.lastBid = " ";
                auction.state = AuctionState.READY;
                db.Auctions.Add(auction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(auction);
        }

        // GET: Auctions/Edit/5
        [System.Web.Mvc.Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }

            if(auction.state == AuctionState.OPEN)
            {
                return RedirectToAction("Index");
            }
            return View(auction);
        }

        // POST: Auctions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,productName,duration,starting_price,create_datetime,open_datetime,close_datetime,deleted,state,increment,imageToUpload")] Auction auction)
        {
            if(ModelState.IsValid)
            {
                Auction a = db.Auctions.Find(auction.id);
                if (auction.imageToUpload != null)
                {
                    auction.image = new byte[auction.imageToUpload.ContentLength];
                    auction.imageToUpload.InputStream.Read(auction.image, 0, auction.image.Length);
                    a.image = auction.image;
                }
                a.duration = auction.duration;
                a.productName = auction.productName;
                a.starting_price = auction.starting_price;
                a.increment = auction.increment;

                db.Entry(a).CurrentValues.SetValues(db.Auctions);
                db.SaveChanges();

                return RedirectToAction("Index");

            }

            return View(auction);
        }

        // GET: Auctions/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }

            if(auction.state == AuctionState.OPEN)
            {
                return RedirectToAction("Index");
            }
            return View(auction);
        }

        // POST: Auctions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Auction auction = db.Auctions.Find(id);
            auction.deleted = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void placePid(string userName, string idAuction)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<BidHub>();
            ApplicationDbContext db = new ApplicationDbContext();

            BidInfo bidInfo = new BidInfo();
            Bid bid = new Bid();

            bidInfo.idAuction = idAuction;
            bidInfo.last10Bids[0] = userName;

            long id = long.Parse(idAuction);

            Auction auction = db.Auctions.Find(id);
            bidInfo.increment = auction.increment;
            auction.starting_price += auction.increment;
            var users = from n in db.Users select n;

            users = users.Where(s => s.UserName == userName);

            ApplicationUser user = users.First();

            if (user.tokens < 1)
            {
                context.Clients.All.noMoreTokens(userName, id);
                return;
            }
            else
                context.Clients.All.pingFormSucc(id);

            user.tokens--;

            List<ApplicationUser> list = users.ToList();

            bid.idAuction = auction.id;

            DateTime t = auction.close_datetime.Value;
            int time_left = (int)auction.close_datetime.Value.Subtract(DateTime.Now).TotalSeconds;
            if(time_left <= 10)
            {
                long pom = 10 - time_left;
                t = auction.close_datetime.Value.AddSeconds(pom);
            }

            auction.close_datetime = t;

            auction.lastBid = userName;

            bid.create_datetime = DateTime.Now;
            bid.idUser = list.First().Id;

            try
            {
                db.Entry(auction).CurrentValues.SetValues(db.Auctions);
                db.Bids.Add(bid);
                db.Entry(user).CurrentValues.SetValues(db.Users);
                db.SaveChanges();
            } catch(DbUpdateConcurrencyException exception)
            {
                context.Clients.All.failedBid(userName);
                return;
            }

            long idAuc = long.Parse(idAuction);
            var bids = from n in db.Bids select n;
            bids = bids.Where(s => s.idAuction == idAuc).OrderBy(k => k.create_datetime);
            List<string> last10Users = new List<string>();
            last10Users = bids.Select(m => m.idUser.ToString()).ToList();
            last10Users.Reverse();

            for (int i = 0; i < 10 && last10Users.Count > i; i++)
            {
                ApplicationUser u = db.Users.Find(last10Users[i]);
                bidInfo.last10Bids[i] = u.UserName;
            }

            bidInfo.last10Bids[0] = userName;

            context.Clients.All.answerBid(bidInfo);
        }

        [Microsoft.AspNet.SignalR.Authorize(Roles ="Admin")]
        public long open(long id)
        {
            Auction auction = db.Auctions.Find(id);

            auction.open_datetime = DateTime.Now;

            DateTime time = (DateTime)auction.open_datetime;
            auction.close_datetime = time.AddSeconds((double)auction.duration);
            auction.state = AuctionState.OPEN;
            auction.lastBid = "No bid";
            db.Entry(auction).CurrentValues.SetValues(db.Auctions);
            db.SaveChanges();

            Task.Factory.StartNew(() =>
            {
                ApplicationDbContext newDb = new ApplicationDbContext();
                Auction a = newDb.Auctions.Find(id);
                while (true)
                {
                    int fullTime = (int)a.close_datetime.Value.Subtract(DateTime.Now).TotalSeconds;
                    if (fullTime > 0)
                    {
                        Thread.Sleep((int)fullTime * 1000); // sleep till auciton is out of time
                    }
                    else
                        break;
                }

                var context = GlobalHost.ConnectionManager.GetHubContext<BidHub>();
                a = newDb.Auctions.Find(id);


                var bids_var = db.Bids.Where(s => s.idAuction == id);
                Array bids = bids_var.ToArray();
                if(bids.Length == 0)
                {
                    auction.state = AuctionState.EXPIRED;
                    context.Clients.All.closeAuction(id, 0);
                } else
                {
                    auction.state = AuctionState.SOLD;
                    context.Clients.All.closeAuction(id, 1);
                }
                db.Entry(auction).CurrentValues.SetValues(db.Auctions);
                db.SaveChanges();
            });

            return (long) auction.duration;
        }
    }
}
