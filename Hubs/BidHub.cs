﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using WebApplication1.Controllers;
using WebApplication1.Models;
using System.Threading.Tasks;

namespace WebApplication1.Hubs
{
    public class BidHub : Hub
    {
        static private AuctionsController ac = new AuctionsController();

        public void Hello()
        {
            Clients.All.hello();
        }

        public void OpenAuction(long id)
        {
            long duration = ac.open(id);
            Clients.All.auctionIsOpen(id, duration);
        }

        public void TakeBid(string idUser, string idAuction)
        {
            ac.placePid(idUser, idAuction);
        }

        public void AnswerBid(BidInfo bidInfo)
        {
            Clients.All.AnswerBid(bidInfo);
        }

        public void CloseAuction(long id, int sold)
        {
            Clients.All.CloseAuction(id, sold);
        }
    }
}