﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebApplication1.Models
{
    [Table("Bid")]
    public class Bid
    {
        [Key]
        public int idBid { get; set; }
        public DateTime create_datetime { get; set; }

        public string idUser { get; set; }

        public int idAuction { get; set; }

        public virtual Auction auction { get; set; }

        public virtual ApplicationUser user { get; set; }

    }

    public class BidInfo
    {
        public string idAuction { get; set; }
        public string[] last10Bids { get; set; }
        public double increment { get; set; }

        public BidInfo()
        {
            last10Bids = new string[10];
        }
    }
}