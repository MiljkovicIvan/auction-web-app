﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebApplication1.Models
{
    [Table("Order")]
    public class Order { 

        public enum OrderState
        {
            PROCESSING, ACCEPTED, DENIED
        }

        [Key]
        public int id { get; set; }

        public int? num_of_tokens { get; set; }

        public double price { get; set; }

        public OrderState state { get; set; }

        public DateTime create_datetime { get; set; }


        public string idUser { get; set; }
        public virtual ApplicationUser user { get; set; }

    }
}