﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{

    public enum AuctionState
    {
        DRAFT, READY, OPEN, SOLD, EXPIRED
    }


    [Table("Auction")]
    public partial class Auction
    {
        [Key]
        public int id { get; set; }
        public string productName { get; set; }
        public double duration { get; set; }
        public double starting_price { get; set; }
        public DateTime? create_datetime { get; set; }
        public DateTime? open_datetime { get; set; }
        public DateTime? close_datetime { get; set; }
        public bool deleted { get; set; }
        public AuctionState? state { get; set; }
        public double increment { get; set; }
        public string lastBid { get; set; }
        public byte[] image { get; set; }
        [Timestamp]
        public virtual ICollection<Bid> bids { get; set; }

        [NotMapped]
        public HttpPostedFileBase imageToUpload { get; set; }
    }

    public class AuctionDetailModelView
    {
        public Auction auction { get; set; }
        public string[] bids { get; set; }

        public AuctionDetailModelView ()
        {
            this.bids = new string[10];
        }
    }
}