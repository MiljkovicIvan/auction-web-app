﻿var bidHub = $.connection.bidHub;
$.connection.hub.start();

function openAuction(id) {
    bidHub.server.openAuction(id);
}

function bid(idU, idA) {
    bidHub.server.takeBid(idU, idA);
    // check tokens. pingFormSucc or pingFormFail
}

// red color FF8080
// default color f5f5f5
// green color ccffcc

function pingFormSucc(id) {
    document.getElementById("formHolder-" + id).style.backgroundColor = "#ccffcc";

    setTimeout(function () { document.getElementById("formHolder-" + id).style.backgroundColor = "#f5f5f5" }, 200);
}

function pingFormFail(id) {
    document.getElementById("formHolder-" + id).style.backgroundColor = "#ffcccc";

    setTimeout(function () { document.getElementById("formHolder-" + id).style.backgroundColor = "#f5f5f5" }, 200);
}

function showUsers(bid) {
    var firstUser   = 'user1-'  + bid.idAuction;
    var secondUser  = 'user2-'  + bid.idAuction;
    var thirdUser   = 'user3-'  + bid.idAuction;
    var fourthUser  = 'user4-'  + bid.idAuction;
    var fifthUser   = 'user5-'  + bid.idAuction;
    var sixthUser   = 'user6-'  + bid.idAuction;
    var seventhUser = 'user7-'  + bid.idAuction;
    var eighthUser  = 'user8-'  + bid.idAuction;
    var ninthUser   = 'user9-'  + bid.idAuction;
    var tenthUser   = 'user10-' + bid.idAuction;


    var pom1 = document.getElementById(firstUser);
    if (pom1 != null)
        var u1 = document.getElementById(firstUser);

    var pom2 = document.getElementById(secondUser);
    if (pom2 != null)
        var u2 = document.getElementById(secondUser);

    var pom3 = document.getElementById(thirdUser);
    if (pom3 != null)
        var u3 = document.getElementById(thirdUser);

    var pom4 = document.getElementById(fourthUser);
    if (pom4 != null)
        var u4 = document.getElementById(fourthUser);

    var pom5 = document.getElementById(fifthUser);
    if (pom5 != null)
        var u5 = document.getElementById(fifthUser);

    var pom6 = document.getElementById(sixthUser);
    if (pom6 != null)
        var u6 = document.getElementById(sixthUser);

    var pom7 = document.getElementById(seventhUser);
    if (pom7 != null)
        var u7 = document.getElementById(seventhUser);

    var pom10 = document.getElementById(tenthUser);
    if (pom10 != null)
        var u10 = document.getElementById(tenthUser);

    var pom8 = document.getElementById(eighthUser);
    if (pom8 != null)
        var u8 = document.getElementById(eighthUser);

    var pom9 = document.getElementById(ninthUser);
    if (pom9 != null)
        var u9 = document.getElementById(ninthUser);

    if (u1 != null) {
        u1.style.visibility = 'visible';
        u1.innerHTML = bid.last10Bids[0];
    }
    if (u2 != null) {
        u2.style.visibility = 'visible';
        u2.innerHTML = bid.last10Bids[1];
    }
    if (u3 != null) {
        u3.style.visibility = 'visible';
        u3.innerHTML = bid.last10Bids[2];
    }
    if (u4 != null) {
        u4.style.visibility = 'visible';
        u4.innerHTML = bid.last10Bids[3];
    }
    if (u5 != null) {
        u5.style.visibility = 'visible';
        u5.innerHTML = bid.last10Bids[4];
    }
    if (u6 != null) {
        u6.style.visibility = 'visible';
        u6.innerHTML = bid.last10Bids[5];
    }
    if (u7 != null) {
        u7.style.visibility = 'visible';
        u7.innerHTML = bid.last10Bids[6];
    }
    if (u8 != null) {
        u8.style.visibility = 'visible';
        u8.innerHTML = bid.last10Bids[7];
    }
    if (u9 != null) {
        u9.style.visibility = 'visible';
        u9.innerHTML = bid.last10Bids[8];
    }
    if (u10 != null) {
        u10.style.visibility = 'visible';
        u10.innerHTML = bid.last10Bids[9];
    }
}

bidHub.client.pingFormSucc = function (id) {
    pingFormSucc(id);
}

bidHub.client.failedBid = function (UserName) {
    var currentUser = document.getElementById(UserName);
    if (currentUser == null) return;
    alert("Failed Bid");


}

bidHub.client.noMoreTokens = function (UserName, id) {
    var currentUser = document.getElementById(UserName);
    if (currentUser == null) return;
    pingFormFail(id);
}

bidHub.client.answerBid = function (bid) {
    showUsers(bid);
    var timeOrState = "timeStateHolder-" + bid.idAuction;
    var buttonOpen = "openBtnHolder-" + bid.idAuction;
    var buttonBid = "bidBtnHolder-" + bid.idAuction;
    var lastBid = "lastBidHolder-" + bid.idAuction;
    var priceHolder = "priceHolder-" + bid.idAuction;

    var price = document.getElementById(priceHolder).innerHTML;
    price = + price + bid.increment;
    document.getElementById(priceHolder).innerHTML = price;
    var duration = document.getElementById(timeOrState).innerHTML;

    if (duration != null) {
        var time = +duration;
        if (time <= 10) {
            duration = +10;
        }
    }

    var pom2 = document.getElementById(timeOrState);
    if (pom2 != null && duration != null)
        document.getElementById(timeOrState).innerHTML = duration;

    var pom = document.getElementById(lastBid);
    if (pom != null)
        document.getElementById(lastBid).style.visibility = 'visible';

    var pom1 = document.getElementById(lastBid);
    if (pom1 != null)
        document.getElementById(lastBid).innerHTML = bid.last10Bids[0];

}


bidHub.client.auctionIsOpen = function (id, duration) {

    var timeOrState = "timeStateHolder-" + id;
    var buttonOpen = "openBtnHolder-" + id;
    var buttonBid = "bidBtnHolder-" + id;
    var lastBid = "lastBidHolder-" + id

    var x = document.getElementById(buttonOpen);
    if (x != null) { x.style.visibility = 'hidden'; }

    var pom1 = document.getElementById(timeOrState);
    if (pom1 != null) {
        document.getElementById(timeOrState).innerHTML = duration;
    }

    var pom2 = document.getElementById(buttonBid);
    if (pom2 != null) {
        document.getElementById(buttonBid).style.visibility = 'visible';
    }


    window['timer_' + id] = setInterval(function () {
        var time = document.getElementById(timeOrState).innerHTML;
        if (time != null) {
            if (time > 0) time--;
            document.getElementById(timeOrState).innerHTML = time;
        }
    }, 1000);


}

bidHub.client.closeAuction = function (id, isSold) {
    var timeOrState = "timeStateHolder-" + id;
    var buttonOpen = "openBtnHolder-" + id;
    var buttonBid = "bidBtnHolder-" + id;
    var lastBid = "lastBidHolder-" + id;

    var pom = document.getElementById(buttonBid);
    if (pom != null)
        document.getElementById(buttonBid).style.visibility = 'hidden';

    if (isSold == 0) {


        var pom3 = document.getElementById(timeOrState);
        if (pom3 != null) {
            document.getElementById(timeOrState).innerHTML = 'EXPIRED';
            document.getElementById(timeOrState).className = 'btn-danger btn btn-block';
        }


    }
    else if (isSold == 1) {

        var pom5 = document.getElementById(timeOrState);
        if (pom5 != null) {
            document.getElementById(timeOrState).innerHTML = 'SOLD';
            document.getElementById(timeOrState).className = 'btn-danger btn btn-block';
        }

        var buttonO = document.getElementById(buttonOpen);
        if (buttonO != null) {
            document.getElementById(buttonOpen).style.visibility = 'hidden';
        }
    }
}